<div id="minecraft-status" class="minecraft-status">
  <div class="minecraft-status-inner">
    <div class="ip-title">
      Server IP:
    </div>
    <div class="ip-content">
      <?php echo $variables['minecraft_status']['ip'];?>
    </div>
    <div class="online-indicator online-indicator-<?php echo strtolower($variables['minecraft_status']['status']);?>">
      <?php echo $variables['minecraft_status']['status'];?>
    </div>
    <div class="server-details">
      <div class="player-count-title">
        Players:
      </div>
      <div class="player-count">
        <?php echo $variables['minecraft_status']['player_count'] . ' / ' . $variables['minecraft_status']['max_players'];?>
      </div>
      <div class="version-title">
        Version:
      </div>
      <div class="version">
        <?php echo $variables['minecraft_status']['version'];?>
      </div>
      <div class="last-updated-title">
        As Of:
      </div>
      <div class="last-updated">
        <?php print (new DateTime())->format('m-d-Y @ H:i:s');?>
      </div>
    </div>
  </div>
</div>