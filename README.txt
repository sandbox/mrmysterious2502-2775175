INTRODUCTION
------------

Displays the status of a minecraft server in a block. Display includes the ip 
of the server, the number of maximum and online players, an online indicator, 
and the version of the server. Capable of using the Minecraft ping protocol 
(Pre and post 1.7) as well as the Gamespy4 protocol.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/minecraft_status

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/minecraft_status

REQUIREMENTS
------------

This module requires the following modules:

 * Minecraft Query (https://drupal.org/project/minecraft_query)

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 
 * On the "Blocks" page, enable the "Minecraft Status" block and then click 
   configure:

   - Server IP: The Minecraft server's ipv4 address (or domain, i.e. 
     mc.dimension-seven.net).

   - Server Port: The port that the Minecraft server is listening on.

   - Server Protocol: The "language" that the server speaks. If you don't know, 
     choose "Minecraft Ping".
